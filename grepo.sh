#!/bin/bash

# https://gitlab.com/locket/grepo

# https://git-scm.com/docs/git-config
cat >.grepo <<END
[commit]
	gpgSign = true
[log]
	showSignature = true
[tag]
	forceSignAnnotated = true
[branch "master"]
	mergeoptions = -S --verify-signatures --no-ff
[branch "dev"]
	mergeoptions = -S --no-ff
END

git config --local include.path ./.grepo # https://stackoverflow.com/a/18330114/2374223