# Grepo
A tool that leverages git hooks and a customized `.gitconfig` to help secure repos using GPG signatures.

The name originally came from merging "gpg" and "repo", but works especially well because ["grepo"](https://en.wikipedia.org/wiki/Grepo) was also a nickname for the German word "Grenzpolizei", meaning "border police", which is essentially what this library strives to do.